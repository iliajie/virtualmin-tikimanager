use strict;
use warnings;
our (%config);
our (%text);

BEGIN { push(@INC, ".."); };

use Cwd qw(getcwd);
use WebminCore;
use File::Basename;
use File::Copy qw(move);
use File::Path qw(make_path);
use File::Fetch;
use File::Find;
use IO::Handle;
use IPC::Open3;

# apt-get install libdbd-sqlite3-perl
# yum install perl-DBD-SQLite
eval('use DBI;');

# apt-get install libjson-perl
# yum install perl-JSON
eval('use JSON;');

# apt-get install libyaml-tiny-perl
# yum install perl-YAML-Tiny
eval('use YAML::Tiny;');

&init_config();
&foreign_require("virtual-server", "virtual-server-lib.pl");
&foreign_require("useradmin");


#
# Install cron entries to run tiki
#
sub tikimanager_cron_setup
{
  my ($d) = @_;
  &foreign_require("cron");

  my %manager = &tikimanager_info();
  my ($info) = &tikimanager_tiki_info($d);

  my $command = "$manager{'exec'} instance:console --instances=$info->{'instance_id'} --command=scheduler:run";
  my @freq = ("*/5", "*", "*", "*", "*");

  my @jobs = &cron::list_cron_jobs();
  foreach my $job (@jobs) {
    if ($job->{'command'} =~ /$command/) {
      &cron::delete_cron_job($job);
    }
  }

  my %job = (
    'active'   => 'yes',
    'mins'     => $freq[0],
    'hours'    => $freq[1],
    'days'     => $freq[2],
    'months'   => $freq[3],
    'weekdays' => $freq[4],
    'command'  => $command,
    'user'     => $d->{'user'},
  );
  &cron::insert_cron_job(\%job);
}

#
# Make a command line call to Tiki Manager. It reveivers the command
# string as the first parameter and a callback that will be called for
# every line of the output result.
#
sub tikimanager_execute_command
{
  my ($command, $callback) = @_;
  my %manager = &tikimanager_info();

  my $cmd = "$manager{'exec'} $command";
  $cmd .= " --no-ansi";
  $cmd .= " --no-interaction";
  $cmd .= " 2>&1";

  # open pipe to $cmd output
  open(my $cmdh, '-|', $cmd);

  # read content from the pipe handler $cmdh
  my $out = "$cmd";
  if ($callback) {
    &{$callback}($cmd);
  }

  while (my $line = <$cmdh>) {
    $out .= $line;
    if ($callback) {
      &{$callback}($line);
    }
  }

  # close pipe handler $cmdh
  close($cmdh);
  my $code = $?;

  return ($out, $code);
}

#
# Execute a given SQL and return an array with the statement object
# and the return integer of the operation
#
sub tikimanager_execute_sqlite
{
  my ($query) = @_;
  my %manager = &tikimanager_info();

  my %conn_config;
  $conn_config{'AutoCommit'} = 1;
  $conn_config{'PrintError'} = 0;
  $conn_config{'RaiseError'} = 1;

  my $dbh = DBI->connect("dbi:SQLite:dbname=$manager{'data'}",'','', \%conn_config);
  my $stmt = $dbh->prepare($query);
  my $rv = $stmt->execute();

  return ($stmt, $rv);
}

sub tikimanager_fix_ownership
{
  my ($d) = @_;

  my @to_fix = (
    "$d->{'public_html_path'}",
    "$d->{'home'}/tmp",
    $config{'tiki_config_file'} || "../tikiconfig/config.php",
    $config{'tiki_preferences_file'} || "../tikiconfig/prefs.ini"
  );

  my $oldpath = getcwd();
  chdir($d->{'public_html_path'});

  for my $file (@to_fix) {
    if ( -e "$file" ) {
      system('chown -Rh' . "$file");
    }
  }

  chdir($oldpath);
}

#
# Generate a random 12 length password composed by alphanumeric chars
#
sub tikimanager_generate_password
{
  my @alphanumeric = ('a'..'z', 'A'..'Z', 0..9);
  return join('', map($alphanumeric[rand(@alphanumeric)], 0..12));
}

#
# Return a list of installable branches
#
sub tikimanager_get_installable_branches
{
  my ($d) = @_;
  my @defaults = ( '18.x', '21.x', '23.x', '24.x', 'master' );

  my $phpversion = &virtual_server::get_domain_php_version($d);
  $phpversion ||= &virtual_server::get_apache_mod_php_version();
  $phpversion = &virtual_server::get_php_version($phpversion) || $phpversion;

  my $json = undef;
  my $err = undef;

  http_download(
    'gitlab.com',                                       # host
    '443',                                              # port
    '/api/v4/projects/tikiwiki%2Ftiki/repository/branches', # path
    \$json,                                             # ref to scalar
    \$err,                                              # ref to scalar
    undef,                                              # callback
    1,                                                  # ssl mode
    undef,                                              # user
    undef,                                              # password
    undef,                                              # timout
    0,                                                  # convert sourceforge links
    0,                                                  # disable webmin cache
    undef                                               # headers hash
  );

  if ( ! $json ) {
    return @defaults;
  }

  $json = decode_json($json);
  push(@{$json}, {'name' => 'master'});

  my @branches;
  foreach my $branch (@{$json}) {
    if (tikimanager_is_branch_installable($branch->{'name'}, $phpversion)){
      push(@branches, $branch->{'name'});
    }
  }
  @branches = sort(@branches); # TODO: find out how to sort for the UI and CLI
  return @branches;
}

#
# Return array with MySQL username, password retrieved from
# virtual $domain object
#
sub tikimanager_get_mysql_domain_credentials
{
  my ($domain) = @_;

  my $d = $domain->{'parent'}
    ? &virtual_server::get_domain($domain->{'parent'})
    : $domain;

  return (
    $d->{'mysql_user'},
    $d->{'mysql_pass'},
  );
}

#
# Return the php interpreter for the domain
#
sub tikimanager_get_php_exec
{
  my ($d) = @_;
  my $phpversion = &virtual_server::get_domain_php_version($d);
  $phpversion ||= &virtual_server::get_apache_mod_php_version();

  return &virtual_server::has_command('php' . $phpversion)
    || &virtual_server::has_command('php');
}


#
# Info about Tiki Manager itself
#
sub tikimanager_info
{
  my %man;
  $man{'home'} = $config{'tikimanager_home'} || "/opt/tiki-manager/app/tiki-manager";
  $man{'exec'} = $config{'tikimanager_exec'} || "$man{'home'}/tiki-manager";
  $man{'data'} = $config{'tikimanager_data'} || "$man{'home'}/data/trim.db";
  return %man;
}

#
# Give a Tiki version and PHP version available, return whether is
# possible to install or not Tiki
#
sub tikimanager_is_branch_installable
{
  my ($branch, $phpversion) = @_;
  my %manager = &tikimanager_info();

  my $yaml = YAML::Tiny->read("$manager{'home'}/config/tiki_requirements.yml");
  my @rules = sort { $b->{'version'} <=> $a->{'version'} }  @{ $yaml->[0] };

  my $previous = {
    'version' => $rules[0]->{'version'} + 1,
    'min'     => $rules[0]->{'php'}->{'min'},
    'max'     => $rules[0]->{'php'}->{'max'}
  };

  if ($branch eq 'master') {
    $branch = $previous->{'version'};
  }

  $branch =~ s/^(\d+).*/$1/;
  $branch = int($branch);

  $phpversion =~ s/^(\d+)(\.\d+)?.*/$1$2/;
  $phpversion = $phpversion * 1;

  my $supported = 0;
  foreach my $rule (@rules) {
      $supported = $phpversion >= $rule->{'php'}->{'min'}
          && (
            !exists($rule->{'php'}->{'max'})
            || $phpversion <= $rule->{'php'}->{'max'}
          )
          && (
              $branch <= $previous->{'version'}
          )
          && $branch >= $rule->{'version'};

      if ($supported) {
          return $supported;
      }
      $previous = $rule;
  }
  return 0;
}

#
# Call Tiki Manager to clone a Tiki instance
#
sub tikimanager_tiki_clone
{
  my ($d, $source, $callback) = @_;
  my ($mysql_user, $mysql_pass) = &tikimanager_get_mysql_domain_credentials($d);
  my ($info) = &tikimanager_tiki_info($d);

  my $cmd = "instance:clone";
  $cmd .= " --source=$source";
  $cmd .= " --target=$info->{'instance_id'}";
  $cmd .= " --direct";
  $cmd .= " --db-host=localhost";
  $cmd .= " --db-user='$mysql_user'";
  $cmd .= " --db-pass='$mysql_pass'";
  $cmd .= " --db-name='$d->{'db_mysql'}'";
  
  return &tikimanager_execute_command($cmd, $callback);
}

#
# Call Tiki Manager to clone a Tiki instance
#
sub tikimanager_tiki_cloneandupgrade
{
  my ($d, $source, $branch, $callback) = @_;
  my ($mysql_user, $mysql_pass) = &tikimanager_get_mysql_domain_credentials($d);
  my ($info) = &tikimanager_tiki_info($d);

  my $cmd = "instance:cloneandupgrade";
  $cmd .= " --source=$source";
  $cmd .= " --target=$info->{'instance_id'}";
  $cmd .= " --branch=$branch";
  $cmd .= " --direct";
  $cmd .= " --db-host=localhost";
  $cmd .= " --db-user='$mysql_user'";
  $cmd .= " --db-pass='$mysql_pass'";
  $cmd .= " --db-name='$d->{'db_mysql'}'";
  
  return &tikimanager_execute_command($cmd, $callback);
}
#
# Just remove the entry from tiki-manager
#
sub tikimanager_tiki_delete
{
  my ($d, $callback) = @_;
  my ($info) = &tikimanager_tiki_info($d);

  my $cmd = "instance:delete";
  $cmd .= " --instances=$info->{'instance_id'}";

  return &tikimanager_execute_command($cmd, $callback);
}

#
# Returns a hash with local.php information
#
sub tikimanager_tiki_get_config
{
  my ($d) = @_;
  my $php_code = <<PHP
<?php
    call_user_func(function() {
      include('$d->{'public_html_path'}/db/local.php');
      echo json_encode(get_defined_vars());
    });
PHP
;
  my $php_in;
  my $php_out;
  my $pid = open3($php_in, $php_out, '>&STDERR', '/usr/bin/php');

  print $php_in $php_code;
  close($php_in);

  my $content = '';
  while (my $line = <$php_out>) {
    $content .= $line;
  }
  close($php_out);

  return decode_json($content);
}


#
# Call Tiki Manager to import an installed Tiki
#
sub tikimanager_tiki_import
{
  my ($d, $callback) = @_;

  my $cmd = " instance:import";
  $cmd .= " --type=local";
  $cmd .= " --url='https://$d->{'dom'}'";
  $cmd .= " --email='$d->{'emailto'}'";
  $cmd .= " --name=$d->{'dom'}";
  $cmd .= " --webroot='$d->{'public_html_path'}'";
  $cmd .= " --tempdir='$d->{'home'}/tmp/tiki_mgr'";
  $cmd .= " --backup-user='$d->{'user'}'";
  $cmd .= " --backup-group='$d->{'ugroup'}'";
  $cmd .= " --backup-permission=750";

  return &tikimanager_execute_command($cmd, $callback);
}

#
# Fetch Tiki information from Tiki Manager and return
# an array having in the first position, a hash with
# tiki information and in the second position, it has
# error messagens with any.
#
sub tikimanager_tiki_info
{
  my ($d) = @_;
  my $error = undef;
  my %result;

  my $domain = $d->{'dom'};

  if (! $domain) {
    $error = "No domain provided";
    return (%result, $error);
  }

  my $query = <<SQL
    SELECT
      i.instance_id,
      i.name,
      i.contact,
      i.webroot,
      i.tempdir,
      i.phpexec,
      v.version_id,
      v.type,
      v.branch,
      v.date,
      v.revision
    FROM instance i
    LEFT JOIN version v
      on i.instance_id = v.instance_id
    WHERE i.name = '$domain'
    ORDER BY v.version_id DESC LIMIT 1;
SQL
;
  my ($stmt, $rv) = &tikimanager_execute_sqlite($query);

  if ($rv < 0) {
    $error = $DBI::errstr;
  } else {
    my @row = $stmt->fetchrow_array();
    if ($row[0]) {
      $result{'instance_id'} = $row[0];
      $result{'name'}        = $row[1];
      $result{'contact'}     = $row[2];
      $result{'webroot'}     = $row[3];
      $result{'tempdir'}     = $row[4];
      $result{'phpexec'}     = $row[5];
      $result{'version_id'}  = $row[6];
      $result{'type'}        = $row[7];
      $result{'branch'}      = $row[8];
      $result{'date'}        = $row[9];
      $result{'revision'}    = $row[10];
    }
    else {
      $error = 'Instance not found';
    }
  }

  return (\%result, $error);
}

#
# Call Tiki Manager to install Tiki
#
sub tikimanager_tiki_install
{
  my ($d, $branch, $callback) = @_;
  my ($mysql_user, $mysql_pass) = &tikimanager_get_mysql_domain_credentials($d);
  my $phpexec = &tikimanager_get_php_exec($d);

  my $cmd = "instance:create";
  $cmd .= " --type=local";
  $cmd .= " --url='https://$d->{'dom'}'";
  $cmd .= " --email='$d->{'emailto'}'";
  $cmd .= ($branch eq 'blank')
    ? " --blank"
    : " --branch='$branch'";
  $cmd .= " --force";
  $cmd .= " --phpexec='$phpexec'";
  $cmd .= " --name=$d->{'dom'}";
  $cmd .= " --webroot='$d->{'public_html_path'}'";
  $cmd .= " --tempdir='$d->{'home'}/tmp/tiki_mgr'";
  $cmd .= " --backup-user='$d->{'user'}'";
  $cmd .= " --backup-group='$d->{'ugroup'}'";
  $cmd .= " --backup-permission=750";
  $cmd .= " --db-host=localhost";
  $cmd .= " --db-user='$mysql_user'";
  $cmd .= " --db-pass='$mysql_pass'";
  $cmd .= " --db-name='$d->{'db_mysql'}'";

  return &tikimanager_execute_command($cmd, $callback);
}

sub tikimanager_tiki_install_and_config
{
    my ($d, $config, $callback) = @_;
    &tikimanager_tiki_install($d, $config->{'branch'}, $callback);
    &tikimanager_tiki_user_password($d, 'admin', $config->{'password'}, $callback);
    &tikimanager_tiki_preference_set($d, 'sender_email', $config->{'sender_email'}, $callback);
    &tikimanager_tiki_set_initial_email($d, $config->{'admin_email'});
    &tikimanager_tiki_preference_set($d, 'fgal_preserve_filenames', 'y', $callback);
    &tikimanager_tiki_setup_storage_folders($d, undef, $callback);

    # fix ownership
    &tikimanager_fix_ownership($d);
}

#
# Check if tikimanager can install Tiki for this domain
#
sub tikimanager_tiki_is_installable
{
  my ($d) = @_;
  my %manager = &tikimanager_info();
  if (-x "$manager{'exec'}") {
    return 0;
  }
  return &text('feat_deps_tikimanager_path', $config{'tikimanager'});
}

#
# Check if Tiki is already installed in tikimanager
#
sub tikimanager_tiki_is_installed
{
  my ($d) = @_;
  my @files_to_check = (
    "$d->{'public_html_path'}/db/local.php",
    "$d->{'public_html_path'}/console.php"
  );
  my $installed = 1;
  foreach my $file (@files_to_check) {
    $installed &= -e $file;
  }
  return $installed;
}

#
# Check if Tiki is already registered in Tiki Manager
#
sub tikimanager_tiki_is_registered
{
  my ($d) = @_;
  return 0;
}

#
# List all Tiki installed Tiki instances possible to be cloned, checking
#
# * PHP version of this virtualserver
# * User has required privileges over source Tiki
#
sub tikimanager_tiki_list_cloneables
{
  my ($d) = @_;

  my $phpversion = &virtual_server::get_domain_php_version($d);
  $phpversion ||= &virtual_server::get_apache_mod_php_version();
  $phpversion = &virtual_server::get_php_version($phpversion) || $phpversion;

  my $query = <<SQL
    SELECT
      i.instance_id,
      i.name,
      v.branch
    FROM instance i
    INNER JOIN version v
      on i.instance_id = v.instance_id
    GROUP BY i.instance_id
    HAVING v.version_id=MAX(v.version_id)
    ORDER BY i.name ASC;
SQL
;

  my @result;
  my ($stmt, $rv) = &tikimanager_execute_sqlite($query);
  if ($rv < 0) {
    return @result;
  }

  my @row;
  while (@row = $stmt->fetchrow_array()) {
    my %map;
    $map{'instance_id'} = $row[0];
    $map{'name'}        = $row[1];
    $map{'branch'}      = $row[2];
    $map{'d'}           = &virtual_server::get_domain($map{'name'});

    my $can_clone = $d->{'dom'} ne $map{'name'}
      && &virtual_server::can_edit_domain($map{'d'})
      && &tikimanager_is_branch_installable($map{'branch'}, $phpversion);

    if ($can_clone) {
      push(@result, \%map);
    }
  }
  return @result;
}

#
# Check possible upgrades
#
sub tikimanager_tiki_list_possible_upgrades
{
  my ($d, $branches) = @_;
  my ($info) = &tikimanager_tiki_info($d);

  if (! $branches) {
    $branches = &tikimanager_get_installable_branches($d);
  }

  my @upgrades;
  if ($info->{'branch'} eq 'master') {
    return @upgrades;
  }

  foreach my $branch (@$branches) {
    if ($branch eq 'master' || int($branch) > int($info->{'branch'})) {
      push(@upgrades, $branch);
    }
  }
  return @upgrades;
}

#
# Set tiki preference using Tiki Manager
#
sub tikimanager_tiki_preference_set
{
  my ($d, $pref, $value, $callback) = @_;
  my ($info) = &tikimanager_tiki_info($d);

  if (! $pref) {
    return undef;
  }

  my $cmd = "instance:console";
  $cmd .= " --instances=$info->{'instance_id'}";
  $cmd .= " --command=\"preference:set '$pref' '$value'\"";

  return &tikimanager_execute_command($cmd, $callback);
}

#
# Saves a hash a the config into local.php
#
sub tikimanager_tiki_save_config
{
  my ($d, $tikiconfig) = @_;

  my $oldpath = getcwd();
  chdir($d->{'public_html_path'});

  my $local = 'db/local.php';

  my $php_code = '<?php' . "\n";
  foreach my $key (sort keys %$tikiconfig)
  {
    my $value = $tikiconfig->{$key} =~ s/'/\\'/r;
    if ($key eq 'system_configuration_file') {
      $php_code .= "  if (is_readable('$value')) {\n";
      $php_code .= "    \$$key = '$value';\n";
      $php_code .= "  } else {\n";
      $php_code .= "    trigger_error('Ini file not found: $value', E_USER_WARNING);\n";
      $php_code .= "  }\n";
    } else {
      $php_code .= "  \$$key = '$value';\n";
    }
  }

  open(my $handler, '|-', '/usr/bin/php', '-nl');
  print $handler $php_code;

  if ($php_code !~ /^\s*$/ && close($handler)) {
    open($handler, '>', $local);
    print $handler $php_code;
    my $result = close($handler);
    chown($d->{'uid'}, $d->{'gid'}, $local);
    chdir($oldpath);
    return $result;
  }

  return 0;
}

sub tikimanager_tiki_set_initial_email
{
  my ($d, $email, $callback) = @_;
  my $query = "UPDATE users_users SET email='$email' WHERE login='admin';";
  return &virtual_server::execute_dom_sql($d, $d->{'db_mysql'}, $query);
}

sub tikimanager_tiki_setup_storage_folders
{
  my ($d, $base, $callback) = @_;

  my $oldpath = getcwd();
  chdir($d->{'public_html_path'});

  if ( ! $base ) {
    # relative to TIKI_ROOT
    $base = "../tikifiles";
  }

  if ( ! -d "$base" ) {
    mkdir($base);
  }

  my %folders;
  $folders{"fgal_batch_dir"} = "${base}/fgal_batch_dir";
  mkdir ($folders{'fgal_batch_dir'});
  tikimanager_tiki_preference_set($d, "fgal_batch_dir", "${base}/fgal_batch_dir", $callback);

  foreach my $prefix (('fgal', 'gal', 't', 'uf', 'w')) {
    my $pref = "${prefix}_use_dir";
    $folders{$pref} = "${base}/${pref}";

    mkdir($folders{$pref});
    tikimanager_tiki_preference_set($d, "${prefix}_use_db", "n", $callback);
    tikimanager_tiki_preference_set($d, $pref, $folders{$pref}, $callback);
  }
  system("chown -R $d->{'user'}:$d->{'user'} $base");
  chdir($oldpath);
  return %folders;
}

#
# Set tiki password
#
sub tikimanager_tiki_user_password
{
  my ($d, $username, $password, $callback) = @_;
  my ($info) = &tikimanager_tiki_info($d);

  $password = $password || &tikimanager_generate_password();

  my $cmd = "instance:console";
  $cmd .= " --instances=$info->{'instance_id'}";
  $cmd .= " --command=\"users:password '$username' '$password'\"";

  return &tikimanager_execute_command($cmd, $callback);
}

#
# Set tiki password
#
sub tikimanager_tiki_user_unlock
{
  my ($d, $username, $callback) = @_;
  my ($info) = &tikimanager_tiki_info($d);

  my $cmd = "instance:console";
  $cmd .= " --instances=$info->{'instance_id'}";
  $cmd .= " --command=\"users:unlock '$username'\"";

  return &tikimanager_execute_command($cmd, $callback);
}

#
# TODO: implement
#
sub tikimanager_tiki_uninstall
{
  my ($d) = @_;
  print Dumper($d);

  return 1;
}

#
# Call Tiki Manager to update Tiki
#
sub tikimanager_tiki_update
{
  my ($d, $instance_id, $branch, $callback) = @_;

  my $cmd = "instance:update";
  $cmd .= " --instances=$instance_id";
  $cmd .= " --branch='$branch'";
  $cmd .= " --email='$d->{'emailto'}'";
  $cmd .= " --live-reindex";

  return &tikimanager_execute_command($cmd, $callback);
}

#
# Call Tiki Manager to upgrade Tiki
#
sub tikimanager_tiki_upgrade
{
  my ($d, $instance_id, $branch, $callback) = @_;

  my $cmd = "instance:upgrade";
  $cmd .= " --instances=$instance_id";
  $cmd .= " --branch='$branch'";
  $cmd .= " --live-reindex";

  return &tikimanager_execute_command($cmd, $callback);
}

#
# Check if password is at least 8 characters
#
sub tikimanager_validate_password
{
  my ($password) = @_;
  return "$password" =~ m/^[a-zA-Z0-9*.!@#\$%^&()\[\]:;<>,?\/~_+-=|]{8,32}$/
}


#
# Check if this virtual module meets dependencies
#
sub tikimanager_virtualmin_deps
{
  my ($d) = @_;
  if ($d->{'web'}) {
    return undef;
  }
  else {
    return $text{'feat_deps_web'};
  }

  if ($d->{'mysql'}) {
    return undef;
  }
  else {
    return $text{'feat_deps_mysql'};
  }
}

1;
