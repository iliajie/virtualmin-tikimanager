#!/usr/bin/perl
use strict;
use warnings;

our (%access, %config, %text, %in);
our $module_name;

require './virtualmin-tikimanager-lib.pl';
&foreign_require("cron");
&ReadParse();

my $d;
if ($in{'dom'}) {
  $d = &virtual_server::get_domain($in{'dom'});
}

# Security check
&error_setup($virtual_server::text{'scripts_ierr'});
&virtual_server::can_edit_domain($d) || &error($virtual_server::text{'edit_ecannot'});
&virtual_server::domain_has_website($d) && $d->{'dir'} || &error($virtual_server::text{'scripts_eweb'});

# Protocol may be conditional
my $proto = "https://";
$proto = "http://"
  if (!&virtual_server::domain_has_ssl($d));
# Make link look nice
my $header_link = &ui_link("$proto$d->{'dom'}", &virtual_server::domain_in($d), undef,
    " target=\"blank\"");
# Separate text and domain name
$header_link =~ s/>(?<T>.*?)(<.*?>.*?<\/.*?>)</>$2</;
$header_link = "$+{T}$header_link";

# Page title, must be first UI thing
&ui_print_header($header_link, $text{'index_title'}, "", undef, 1, 1);

#my @tiki_branches = [ map {[ $_, $_ ]} &tikimanager_get_installable_branches($d) ];
my @tiki_branches = &tikimanager_get_installable_branches($d);
my $latest_branch = $tiki_branches[@tiki_branches - 1];
my ($info) = &tikimanager_tiki_info($d);

my @instances = &tikimanager_tiki_list_cloneables($d);
my @cloneables;
foreach my $instance (@instances) {
  push(@cloneables, [
    $instance->{'instance_id'},
    "$instance->{'name'} ($instance->{'branch'})"
  ]);
}

# Define tabs
my $is__actions = $info && $info->{'instance_id'};
my $is__tiki_installed = tikimanager_tiki_is_installed($d);
my $is__cloneables = @cloneables;
my $is__instance_id = $info && $info->{'instance_id'};
my @jobs;
@jobs= &cron::list_cron_jobs()
  if ($is__instance_id);

my $page = "index.cgi?dom=$d->{'dom'}&mode=";
my @tabs = (
    $is__actions ? ([ "actions", "Instance Actions", $page."actions" ]) : ( ),
    $is__actions ? ([ "instanceupdate", "Instance Update", $page."instanceupdate" ]) : ( ),
    !$is__actions && $is__tiki_installed ? ([ "importinstance", "Import Existing Instance", $page."importinstance" ]) : ( ),
    !$is__actions ? ([ "installnew", "Install a New Instance", $page."installnew" ]) : ( ),
    $is__cloneables ? ([ "cloneables", "Clone", $page."cloneables" ]) : ( ),
    $is__instance_id && @jobs ? ([ "cjobs", "Instance Jobs", $page."cjobs" ]) : ( ),
  );

if ($is__actions) {
  #
  # When Tiki is installed by Tiki Manager, show Tiki info
  #

  print &ui_table_start("Information by Tiki Manager", "width=100%", 2);
  print &ui_table_row($text{'tikimanager_instance_id'}, $info->{'instance_id'}, 2);
  print &ui_table_row($text{'tikimanager_webroot'},     $info->{'webroot'}, 2);
  print &ui_table_row($text{'tikimanager_tempdir'},     $info->{'tempdir'}, 2);
  print &ui_table_row($text{'tikimanager_phpexec'},     $info->{'phpexec'}, 2);
  print &ui_table_row($text{'tikimanager_type'},        $info->{'type'}, 2);
  print &ui_table_row($text{'tikimanager_branch'},      $info->{'branch'}, 2);
  print &ui_table_row($text{'tikimanager_date'},        $info->{'date'}, 2);
  print &ui_table_row($text{'tikimanager_revision'},    $info->{'revision'}, 2);
  print &ui_table_end();
  print &ui_hr();

}

print &ui_tabs_start(\@tabs, "mode", $in{'mode'} ||
  ($is__actions ? "actions" : 
    (!$is__actions && $is__tiki_installed ? "importinstance" :
      (!$is__actions ? "installnew" : 
        ($is__cloneables ? "cloneables" : "cjobs") ) )), 1);

if ($is__actions) {

  print &ui_tabs_start_tab("mode", "actions");
  print "<p>Tell user more about what that is #1</p>\n";
  
  print &ui_table_start();
  print &ui_table_row("", "", 2);

  print &ui_table_row(
    "Edit tiki.ini",
    &ui_form_start('./tiki-edit-ini.cgi', 'get', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . &ui_submit("Edit", "edit", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Enforce Tiki preferences hiding sensitive information", 2);
  print &ui_table_row("", "", 2);


  print &ui_table_row(
    "Unlock user",
    &ui_form_start('./tiki-user-reset.cgi', 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . &ui_textbox("user", "", undef, undef, undef, ("placeholder='Username'"))
    . &ui_submit("Unlock", "unlock", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Unlock an user blocked after several login attempts", 2);
  print &ui_table_row("", "", 2);

  print &ui_table_row(
    "Reset password",
    &ui_form_start('./tiki-user-reset.cgi', 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . &ui_textbox("user", "", undef, undef, undef, ("placeholder='Username'"))
    . &ui_textbox("password", &tikimanager_generate_password(), "", undef, undef, ("placeholder='Password'"))
    . &ui_submit("Reset", "reset", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "Set a new password to the specified user", 2);
  print &ui_table_end();
  print &ui_tabs_end_tab();

  print &ui_tabs_start_tab("mode", "instanceupdate");
  print "<p>Tell user more about what that is - #2</p>\n";

  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row(
    "Update Tiki",
    &ui_form_start('./tiki-update.cgi', 'post', undef, ("data=unbuffered-header-processor"))
    . &ui_hidden('dom', $d->{'id'})
    . ui_submit("Update", "update", 0)
    . &ui_form_end(),
  2);
  print &ui_table_row("", "The latest changes of the branch $info->{'branch'} will be applied", 2);

  my @upgradable_to = &tikimanager_tiki_list_possible_upgrades($d, \@tiki_branches);
  if ( @upgradable_to ) {
    print &ui_table_row("", "", 2);
    print &ui_table_row(
      "Upgrade Tiki",
      &ui_form_start('./tiki-upgrade.cgi', 'post', undef, ("data=unbuffered-header-processor"))
      . &ui_hidden('dom', $d->{'id'})
      . &ui_select("branch", $latest_branch, [@upgradable_to])
      . &ui_submit("Upgrade", "upgrade", 0)
      . &ui_form_end(),
    2);
    print &ui_table_row("", "The select version of Tiki will be installed in place of the old one.", 2);
  }
  print &ui_table_row("", "", 2);
  print &ui_table_end();
  print &ui_tabs_end_tab();
}
else {
  if ($is__tiki_installed) {
    print &ui_tabs_start_tab("mode", "importinstance");
    print "<p>Tell user more about what that is #3</p>\n";
    print &ui_form_start('./tiki-import.cgi', 'post', undef, ("data=unbuffered-header-processor"));
    print &ui_hidden('dom', $d->{'id'});
    print &ui_table_start();
    print &ui_table_row("", "", 2);
    print &ui_table_row("Tiki found at:", "'$d->{'public_html_path'}'", 2);
    print &ui_table_row("", "", 2);
    print &ui_table_row("", "Do you want to import it?&nbsp;&nbsp;" . ui_submit("Import", "import", 0), 2);
    print &ui_table_row("", "", 2);
    print &ui_table_end();
    print &ui_form_end();
    print &ui_tabs_end_tab();
  }

  #
  # Install form
  #
  print &ui_tabs_start_tab("mode", "installnew");
  print "<p>Tell user more about what that is #4</p>\n";
  print &ui_form_start('./tiki-install.cgi', 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden('dom', $d->{'id'});
  print &ui_table_start();
  print &ui_table_row("", "", 2);
  print &ui_table_row("Admin email", &ui_textbox('admin_email', $d->{'emailto'}, 48), 2);
  print &ui_table_row("", "Email used by the administrator to receive Tiki alerts and reset password.", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Sender email", &ui_textbox('sender_email', $d->{'emailto'}, 48), 2);
  print &ui_table_row("", "Email to be displayed as 'sender' or 'from' for recipient users receiving emails from this Tiki", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Admin password", &ui_textbox('password', &tikimanager_generate_password()), 2);
  print &ui_table_row("", "Copy or change the password above. You will not see that again.", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select branch", &ui_select("branch", $latest_branch, [@tiki_branches]) . &ui_submit($text{'index_install'}, "save", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();
  print &ui_tabs_end_tab();
}

if ($is__cloneables) {
  #
  # Clone
  #
  print &ui_tabs_start_tab("mode", "cloneables");
  print "<p>Tell user more about what that is #5</p>\n";
  print &ui_form_start('./tiki-clone.cgi', 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden('dom', $d->{'id'});
  print &ui_table_start("Clone from another Tiki instance", "width=100%", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("", &text("index_clone_tiki_message"), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select source", &ui_select("source", undef, [@cloneables]) . ui_submit("Clone", "save", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();

  #
  # Clone and upgrade
  #
  print &ui_form_start('./tiki-cloneandupgrade.cgi', 'post', undef, ("data=unbuffered-header-processor"));
  print &ui_hidden('dom', $d->{'id'});
  print &ui_table_start("Clone from another Tiki instance and then upgrade", "width=100%", 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("", &text("index_clone_tiki_message"), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select source", &ui_select("source", undef, [@cloneables]), 2);
  print &ui_table_row("", "", 2);
  print &ui_table_row("Select branch",  &ui_select("branch", $latest_branch, [@tiki_branches]) . ui_submit("Clone and Upgrade", "cloneandupgrade", 0), 2);
  print &ui_table_row("Warning", &text("index_install_tiki_warning", "$d->{'public_html_path'}"), 2);
  print &ui_table_end();
  print &ui_form_end();  
  print &ui_tabs_end_tab();
}

if ($is__instance_id) {
  if (@jobs) {
    print &ui_tabs_start_tab("mode", "cjobs");
    print "<p>Tell user more about what that is #6</p>\n";
    print &ui_columns_start([
      'Active',
      'Time',
      'Command',
    ]);
    my $idx = 0;
    foreach my $job (@jobs) {
      if ($job->{'user'} eq $d->{'user'}) {
          my @cols;
          push(@cols, $job->{'active'} ? $text{'yes'} : "<font color=#ff0000>$text{'no'}</font>");
          push(@cols, "<tt>$job->{'mins'} $job->{'hours'} $job->{'days'} $job->{'months'} $job->{'weekdays'}</tt>");
          push(@cols, &ui_link("/cron/edit_cron.cgi?idx=" . $idx, $job->{'command'}));
          print &ui_columns_row(\@cols);
      }
      $idx += 1;
    }
    print &ui_columns_end();
    print &ui_tabs_end_tab();
  }
}

print &ui_tabs_end(1);

print <<EOF
<script type="text/javascript">
  jQuery("form[data=unbuffered-header-processor]").on("submit", function(evt) {
    evt.preventDefault();
    unbuffered_header_processor(evt, 1);
    return false;
  });
</script>
EOF
;

&ui_print_footer(
  $d ? &virtual_server::domain_footer_link($d) : ( ),
  "@{[&virtual_server::get_webprefix_safe()]}/virtual-server/",
  $virtual_server::text{'index_return'}
);
