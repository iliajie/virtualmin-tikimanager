# Virtualmin-TikiManager

This virtualmin plugin allows users to manage Tiki Wiki CMS Groupware instances
within Virtualmin Virtual Servers. The plugin code wraps TikiManager command
line calls from Virtualmin web interface.

## Install
### Debian/Ubuntu
```sh
sudo apt-get install apt-add-gitlab
sudo apt-add-gitlab https://packages.wikisuite.org
sudo apt-get install wikisuite-virtualmin-tikimanager
```

More information can be found at https://packages.wikisuite.org/


### From sources
Assuming
* TikiManager is installed at `/opt/tiki-manager/app`
* Webmin is installed at `/usr/share/webmin`

Perl dependencies
```
sudo apt-get \
    install libemail-valid-perl \
    libdbd-sqlite3-perl \
    libjson-perl \
    libyaml-tiny-perl \
    libemail-valid-perl
```

Get the code
```
sudo git clone \
    https://gitlab.com/wikisuite/virtualmin-tikimanager.git \
    /usr/share/webmin/virtualmin-tikimanager
```

Enable the plugin globally
```
virtualmin set-global-feature --enable-feature virtualmin-tikimanager
```

## Cli usage
### Tiki install
```
virtualmin tiki-install --domain test1.example.com --branch 22.x --password tikiwiki
```

## Remote cli usage
### Tiki install
```sh
vmin_domain=test1.example.com
tiki_branch=22.x
tiki_password=tikiwiki

vmin_user=fabio
vmin_pass=********
vmin_host=example.com
vmin_port=10000

url="https://${vmin_host}:${vmin_port}/virtual-server/remote.cgi?json=1&multiline"
url+="&program=tiki-install"
url+="&domain=${vmin_domain}"
url+="&branch=${tiki_branch}"
url+="&password=${tiki_password}"

curl --user "${vmin_user}:${vmin_pass}" "${url}"
```
